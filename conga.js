// wish there was just a global in canvas like controlled
let conga_hovered_token = undefined;
// wish there was a way to just disable hoistory (it's kinda a pain to deal with)
let conga_canvas_history_tracker = 0;

let conga_socket = undefined;

let conga_leader_icon = undefined;
let conga_follower_icon = undefined;

async function conga_follow(leader_document, follower_document) {
	// setup follow links
	await follower_document.setFlag('conga','leader',leader_document.id);
	await leader_document.setFlag('conga','follower',follower_document.id);
}

async function conga_unfollow(leader_document, follower_document) {
	// remove follow link
	await follower_document.unsetFlag('conga','leader');
	await leader_document.unsetFlag('conga','follower');
}

async function conga_insert(a, b, c) {
	// insert follower between new leader and the leader's 'current follower
	await conga_unfollow(a,c);
	await conga_follow(a,b);
	await conga_follow(b,c);
}

async function conga_extract(a, b, c) {
	// extract from between follower's leader and follower's follower
	await conga_unfollow(a,b);
	await conga_unfollow(b,c);
	await conga_follow(a,c);
}

async function conga_token_handle(token_scene_id,leader_document_id,follower_document_id) {
	// should be set
	let token_scene = game.scenes.get(token_scene_id);
	let leader_document = token_scene.tokens.get(leader_document_id);
	let follower_document = token_scene.tokens.get(follower_document_id);
	// potentially unset
	let leader_current_follower_document = token_scene.tokens.get(leader_document.getFlag('conga','follower'));
	let follower_current_leader_document = token_scene.tokens.get(follower_document.getFlag('conga','leader'));
	let follower_current_follower_document = token_scene.tokens.get(follower_document.getFlag('conga','follower'));
	// special case - prevent self following
	if (leader_document.id === follower_document.id) {
		ui.notifications.warn("Conga: token cannot follow itself.");
		return;
	}
	// special case - check for if leader is already the follower's leader or follower is the leader's follower (a waste of processing doing both but I don't care atm)
	let match = false;
	if (follower_current_leader_document !== undefined && leader_document.id === follower_current_leader_document.id || leader_current_follower_document !== undefined && leader_current_follower_document.id === follower_document.id) {
		match = true;
	}
	// just find all possible combinations of unset and check if match in each (drawing out resulting line(s) helped alot)
	if (follower_current_leader_document !== undefined && leader_current_follower_document !== undefined && follower_current_follower_document !== undefined) {
		if (match) {
			await conga_extract(leader_document,follower_document,follower_current_follower_document);
		} else {
			await conga_extract(follower_current_leader_document,follower_document,follower_current_follower_document);
			await conga_insert(leader_document,follower_document,leader_current_follower_document);
		}
	} else if (follower_current_leader_document !== undefined && leader_current_follower_document !== undefined && follower_current_follower_document === undefined) {
		if (match) {
			await conga_unfollow(leader_document,follower_document);
		} else {
			await conga_unfollow(follower_current_leader_document,follower_document);
			await conga_insert(leader_document,follower_document,leader_current_follower_document);
		}
	} else if (follower_current_leader_document !== undefined && leader_current_follower_document === undefined && follower_current_follower_document !== undefined) {
		// a match here is impossible
		await conga_extract(follower_current_leader_document,follower_document,follower_current_follower_document);
		await conga_follow(leader_document,follower_document);
	} else if (follower_current_leader_document === undefined && leader_current_follower_document !== undefined && follower_current_follower_document !== undefined) {
		// a match here is impossible
		await conga_unfollow(follower_document,follower_current_follower_document);
		await conga_insert(leader_document,follower_document,leader_current_follower_document);
	} else if (follower_current_leader_document !== undefined && leader_current_follower_document === undefined && follower_current_follower_document === undefined) {
		// a match here is impossible
		await conga_unfollow(follower_current_leader_document,follower_document);
		await conga_follow(leader_document,follower_document);
	} else if (follower_current_leader_document === undefined && leader_current_follower_document !== undefined && follower_current_follower_document === undefined) {
		// a match here is impossible
		await conga_insert(leader_document,follower_document,leader_current_follower_document);
	} else if (follower_current_leader_document === undefined && leader_current_follower_document === undefined && follower_current_follower_document !== undefined) {
		// a match here is impossible
		await conga_unfollow(follower_document,follower_current_follower_document);
		await conga_follow(leader_document,follower_document);
	} else if (follower_current_leader_document === undefined && leader_current_follower_document === undefined && follower_current_follower_document === undefined) {
		// a match here is impossible
		await conga_follow(leader_document,follower_document);
	}
}

// check if part of a conga and if so remove it cleanly
async function conga_token_delete(token_scene_id, token_document_id) {
	// sould always be set
	let token_scene = game.scenes.get(token_scene_id);
	let token_document = token_scene.tokens.get(token_document_id);
	// potentially unset
	let token_current_leader_document = token_scene.tokens.get(token_document.getFlag('conga','leader'));
	let token_current_follower_document = token_scene.tokens.get(token_document.getFlag('conga','follower'));
	// possible important combinations (only did minimal set's and unset's for flags)
	if (token_current_leader_document !== undefined && token_current_follower_document !== undefined) {
		await token_current_leader_document.setFlag('conga','follower',token_current_follower_document.id);
		await token_current_follower_document.setFlag('conga','leader',token_current_leader_document.id);
	} else if (token_current_leader_document !== undefined && token_current_follower_document === undefined) {
		await token_current_leader_document.unsetFlag('conga','follower');
	} else if (token_current_leader_document === undefined && token_current_follower_document !== undefined) {
		await token_current_follower_document.unsetFlag('conga','leader');
	}
}

async function conga_token_update(token_scene_id, token_document_id, old_x, old_y) {
	let token_scene = game.scenes.get(token_scene_id);
	let token_document = token_scene.tokens.get(token_document_id);
	if (token_document.getFlag('conga','follower') !== undefined) {
		let follower_document = token_scene.tokens.get(token_document.getFlag('conga','follower'));
		if (follower_document !== undefined) {
			token_scene.updateEmbeddedDocuments("Token",[{
				_id: follower_document.id,
				x: old_x,
				y: old_y,
			}]);
		}
	}
}

function conga_show_icons(token) {
	if (!game.settings.get('conga','show_icons')) {
		return
	}
	// works without removing current parent so shrug
	let token_current_leader = canvas.tokens.get(token.document.getFlag('conga','leader'));
	if (token_current_leader !== undefined) {
		if (conga_leader_icon === undefined) {
			conga_leader_icon = new PIXI.Text("L", {fill: "#c0bfbc",fontSize: 60,fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8});
			conga_leader_icon.anchor.set(0.5, 0.5);
			conga_leader_icon.name = "conga";
		}
		if (conga_leader_icon.transform === null) {
			//conga_leader_icon.destroy(); with this commented there may be a mem leak
			conga_leader_icon = new PIXI.Text("L", {fill: "#c0bfbc",fontSize: 60,fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8});
			conga_leader_icon.anchor.set(0.5, 0.5);
			conga_leader_icon.name = "conga";
		}
		token_current_leader.addChild(conga_leader_icon);
		let fontsize = token_current_leader.bounds.width < token_current_leader.bounds.height ? token_current_leader.bounds.width: token_current_leader.bounds.height;
		conga_leader_icon.style = {fill: "#c0bfbc",fontSize: 60*(fontsize/100),fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8};
		conga_leader_icon.x = token_current_leader.bounds.width/2;
		conga_leader_icon.y = token_current_leader.bounds.height/2;
		conga_leader_icon.visible = true;
	}
	let token_current_follower = canvas.tokens.get(token.document.getFlag('conga','follower'));
	if (token_current_follower !== undefined) {
		if (conga_follower_icon === undefined) {
			conga_follower_icon = new PIXI.Text("F", {fill: "#c0bfbc",fontSize: 60,fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8});
			conga_follower_icon.anchor.set(0.5, 0.5);
			conga_follower_icon.name = "conga";
		}
		if (conga_follower_icon.transform === null) {
			//conga_leader_icon.destroy(); with this commented there may be a mem leak
			conga_follower_icon = new PIXI.Text("F", {fill: "#c0bfbc",fontSize: 60,fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8});
			conga_follower_icon.anchor.set(0.5, 0.5);
			conga_follower_icon.name = "conga";
		}
		token_current_follower.addChild(conga_follower_icon);
		let fontsize = token_current_follower.bounds.width < token_current_follower.bounds.height ? token_current_follower.bounds.width: token_current_follower.bounds.height;
		conga_follower_icon.style = {fill: "#c0bfbc",fontSize: 60*(fontsize/100),fontWeight: "bold",lineJoin: "round",stroke: "#241f31",strokeThickness: 8};
		conga_follower_icon.x = token_current_follower.bounds.width/2;
		conga_follower_icon.y = token_current_follower.bounds.height/2;
		conga_follower_icon.visible = true;
	}
	//let icon = new PIXI.Sprite.from("/icons/svg/combat.svg");
}

function conga_hide_icons(token) {
	if (conga_leader_icon !== undefined) {
		conga_leader_icon.visible = false;
		let token_current_leader = canvas.tokens.get(token.document.getFlag('conga','leader'));
		if (token_current_leader === undefined && conga_leader_icon.parent !== null) {
			conga_leader_icon.parent.removeChild(conga_leader_icon);
		}
	}
	if (conga_follower_icon !== undefined) {
		conga_follower_icon.visible = false;
		let token_current_follower = canvas.tokens.get(token.document.getFlag('conga','follower'));
		if (token_current_follower === undefined && conga_follower_icon.parent !== null) {
			conga_follower_icon.parent.removeChild(conga_follower_icon);
		}
	}
}

// create a socket to fun functions as GM
Hooks.once("socketlib.ready", () => {
	conga_socket = socketlib.registerModule('conga');
	// handle token update
	conga_socket.register('conga_token_update', conga_token_update);
	// handle token deletion
	conga_socket.register('conga_token_delete', conga_token_delete);
	// handle user follow/unfollow request
	conga_socket.register('conga_token_handle', conga_token_handle);
});

// hook relevant functions for related token functions
Hooks.once('libWrapper.Ready', () => {
	// should only be only called by the token updator
	libWrapper.register('conga', 'TokenDocument.prototype._preDelete', function (wrapped, ...args) {
		conga_socket.executeAsGM('conga_token_delete',this.parent.id,this.data._id);
		return wrapped(...args);
	}, 'WRAPPER');
	// should only be only called by the token deletor
	libWrapper.register('conga', 'TokenDocument.prototype._preUpdate', function (wrapped, ...args) {
		if (conga_canvas_history_tracker > canvas.tokens.history.length) {
				// undo dected
				conga_canvas_history_tracker = canvas.tokens.history.length;
		} else {
			conga_canvas_history_tracker = canvas.tokens.history.length;
			// ensure that this is an update to position before updating positions
			if (args[0].x !== undefined || args[0].y !== undefined) {

				conga_socket.executeAsGM('conga_token_update',this.parent.id,this.data._id,this.data.x,this.data.y);
			}
		}
		return wrapped(...args);
	}, 'WRAPPER');
	// used to set hovered token (imo this should just be a global thing like controlled)
	libWrapper.register('conga', 'Token.prototype._onHoverIn', function (wrapped, ...args) {
		conga_hovered_token = this.document.id;
		return wrapped(...args);
	}, 'WRAPPER');
	// used to set hovered token (imo this should just be a global thing like controlled)
	libWrapper.register('conga', 'Token.prototype._onHoverOut', function (wrapped, ...args) {
		if (conga_hovered_token === this.document.id) {
			conga_hovered_token = undefined;
		}
		return wrapped(...args);
	}, 'WRAPPER');
	// draw follow and leader marker's on relevant tokens
	libWrapper.register('conga', 'Token.prototype._onControl', function (wrapped, ...args) {
		if (canvas.tokens.controlled.length === 1) {
			conga_hide_icons(canvas.tokens.controlled[0]);
			conga_show_icons(canvas.tokens.controlled[0]);
		} else {
			conga_hide_icons(this);
		}
		return wrapped(...args);
	}, 'WRAPPER');
	// remove follow and leader marker's on relevant tokens
	libWrapper.register('conga', 'Token.prototype._onRelease', function (wrapped, ...args) {
		if (canvas.tokens.controlled.length === 1) {
			conga_hide_icons(canvas.tokens.controlled[0]);
			conga_show_icons(canvas.tokens.controlled[0]);
		} else {
			conga_hide_icons(this);
		}
		return wrapped(...args);
	}, 'WRAPPER');
	// draw follow and leader marker's on relevant tokens
	libWrapper.register('conga', 'Token.prototype._onUpdate', function (wrapped, ...args) {
		if (canvas.tokens.controlled.length === 1) {
			conga_hide_icons(canvas.tokens.controlled[0]);
			conga_show_icons(canvas.tokens.controlled[0]);
		}
		return wrapped(...args);
	}, 'WRAPPER');
	// remove follow and leader marker's on relevant tokens
	libWrapper.register('conga', 'Token.prototype._onDelete', function (wrapped, ...args) {
		if (canvas.tokens.controlled.length === 1) {
			conga_hide_icons(canvas.tokens.controlled[0]);
			conga_show_icons(canvas.tokens.controlled[0]);
		}
		return wrapped(...args);
	}, 'WRAPPER');
});

// hook keybinding
Hooks.once('init', () => {
	const { SHIFT, ALT, CONTROL } = KeyboardManager.MODIFIER_KEYS
	game.keybindings.register('conga', 'conga', {
		name: 'Follow Key',
		hint: 'Pressing this key will try to set a the currently selected token as a follower of another',
		editable: [
			{
			key: 'KeyF',
			},
		],
		onDown: () => {
			if (canvas.tokens.controlled.length == 0) {
				ui.notifications.warn("Conga: no token selected.");
			} else if (canvas.tokens.controlled.length > 1) {
				ui.notifications.warn("Conga: too many tokens selected.");
			} else if (conga_hovered_token === undefined) {
				ui.notifications.warn("Conga: no token to follow being hovered over.");
			} else {
				// send follow change request to a gm to handle
				conga_socket.executeAsGM('conga_token_handle',canvas.tokens.controlled[0].document.parent.id,conga_hovered_token,canvas.tokens.controlled[0].document.id);
			}
		},
	})
	game.settings.register('conga', 'show_icons', {
		name: 'Show Icons for leader and follower',
		hint: 'Show the letters f and l over the follower and leader of the controlled token',
		scope: 'client',
		config: true,
		default: true,
		type: Boolean,
		onChange: value => {
			if (canvas.tokens.controlled.length === 1) {
				conga_hide_icons(canvas.tokens.controlled[0]);
				conga_show_icons(canvas.tokens.controlled[0]);
			}
		}
	});
});

//incoherent notes
// future work
//  advanced conga
//   follow 1 tile behind token (this seems like a major pain tbh)
//    would also have to move to the closest hex/square/distance from(gridless)
//   may require a queue of updates that is a path that wait for animation for finish
//    this would require a custom hook to the end of setposition and would be messy af as that is local client code
//  group changes for better undo support
//   this should be possible by returning an array of changes
//  loop checker to prevent a loop of token
//   not needed with current weird follow join and leave behavior
//  settings
//   option to not let players choose to follow idk
//   add option to only allow to follow friendly tokens (idk if game agnostic way to do so)
//    maybe only allow players to follow other players
//   add option to disable animation
//    { animate: false } as thrid arg to updateEmbeddedDocuments
//  auto remove conga lines for tokens in combat
//   option to leave conga line alone for gm? maybe set combat flag in creation
//  should try and let other players update their own tokens if online and only have gm try and update npcs
//  icon reuse
//   tried this before but token size changes seemed to cause errors and I wanted to push out an update
// know bugs
//  i'm sure undoing causes issues somewhere, somehow
//   if only there was a safe multiple module way of adding more to a single update
//  text centering issues
//  canvas resize may have issues changing icons
//  token resize may have issues changing icons
// other notes
//  this is a proof of concept module for token following inspired loosely by /follow in WoW.
//   I may change the behaiors between versions (will use major update number for that)