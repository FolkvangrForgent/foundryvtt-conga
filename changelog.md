Version 1.0.0
	proof of concept release of conga
Version 1.1.0
	fixed some logic errors
	added first pass of icons (just letters for now)
	added settings menu and first option to disable icons
Version 1.1.1
	added ability for gm to be on any scene and core functionality to still work
	refactored icon logic to mostly usable state