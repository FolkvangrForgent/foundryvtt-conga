Conga is a proof of concept token follow module for FoundryVTT. It currently has basic functionality for token following (tokens move to where leader was). Feel free to open PRs if you want, I have notes at the bottom of conga.js that has ideas for new features.

Default is to use the F key to make the selected token follow or unfollow another token.
If icons  are turned on a f should appear on the follower and a l on the leader the selected token (if multiple tokens are selected they will not appear).

Known Issues
	undoing can cause follows to get messed up when a token is un-deleted
	changing the size of a token will clear the follow indicators until the controlled token is deselected and selected or there is another update.